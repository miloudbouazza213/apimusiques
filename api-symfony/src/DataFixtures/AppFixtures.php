<?php

namespace App\DataFixtures;

use App\Entity\Musique;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        //generer des musiques : 
        //utilisation de faker
        $faker = Factory::create('fr_FR');


        //generer 20 musiques :
        for ($i=0; $i <= 20 ; $i++) { 
                //generer 3 musique par artiste
        $nomArtiste = $faker->name();

        for($j =0 ;$j <=3 ; $j++) {
            $musique = new Musique();
            $musique->setArtiste( $nomArtiste );
            $musique->setTitre($faker->sentence());
            $musique->setDateSortie($faker->dateTime());

            $manager->persist($musique);
        };

        }

     

        
        //generer un admin avec un password hashé

            $admin = new User();
            $admin->setUsername("admin");

//mode de passe : "admin" hashé manueellement avec la commande  :php bin/console security:hash-password
            $admin->setPassword("$2y$13$9nXeEdU5kzKTo6WDPW1b6eHu3jKZD14gwGmqaahx5szxr0LoYZy56");
            $admin->setRoles(["ROLE_ADMIN"]);



        $manager->persist($admin);
        
        $manager->flush();



    }
}
