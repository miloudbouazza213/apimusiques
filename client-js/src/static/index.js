import jwt_decode from "jwt-decode";
console.log("en test")
// // recuperer le token de l'utilisateur si il existe dans la session
// if (sessionStorage.getItem("token")) {
//     var decoded = jwt_decode(sessionStorage.getItem("token"));

//     console.log(decoded.roles.includes("ROLE_ADMIN"))
// }


//----------------variable:
const conteneurPrincipal = document.getElementById("conteneur-principal")
const rowPrincipal = document.getElementById("row-principale")

//modal modif pour ajout et modification:
const modalModifTitre = document.getElementById("titre")
const modalModifArtiste = document.getElementById("artiste")
const modalModifdateSortie = document.getElementById("dateSortie")
const ModalTitre = document.getElementById("modal-modif-titre")

//message
const messageConn = document.getElementById("msg-connexion")
const messageInsc = document.getElementById("msg-inscription")

//pagination
const premierePage = document.getElementById("premierePage")
const pageSuivante = document.getElementById("pageSuivante")
const pagePrec = document.getElementById("pagePrec")
const dernierePage = document.getElementById("dernierePage")


//api
const urlApi = "https://8000-miloudbouazza21-apimusiq-bjchia1mqbw.ws-eu27.gitpod.io"
//utile pour le rafraichisement apres modif ou suppr de musique
var pageCourante = ""

const infoUser = document.getElementById("info-user")
//button
const btnConn = document.getElementById("btnConn")
const btnInsc = document.getElementById("btnInsc")
const btnGestionUsers = document.getElementById("btnGestionUsers")
const btnModifier = document.getElementById("btnModifier")
const btnSearch = document.getElementById("btnSearch")
const btnEffacer = document.getElementById("btnEffacer")

//--------------- fonction utils -------------------------

//-------------------- connexion a l'api avec username et pwd --------------------------


//fonction pour verifier si user est connecté et recuperer ses info
function recupererinfoUserSiConnecte() {
    if (sessionStorage.getItem("token")) {
        var decoded = jwt_decode(sessionStorage.getItem("token"));
        infoUser.innerHTML = "Connecté en tant que " + decoded.username

        //si admin lui donner l'acces au bouton permettant la gestion
        if (decoded.roles.includes("ROLE_ADMIN")) {
            btnGestionUsers.disabled = false
        }
        else {
            btnGestionUsers.disabled = true
        }
    }
    else {
        infoUser.innerHTML = "vous n'etes pas connecté"
    }
}

//verifier si un user est connecté 
recupererinfoUserSiConnecte();

//connecter l'utilisateur en envoyant une requete fetch
async function connecter(username, pwd) {

    btnConn.innerHTML = '  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> connexion...'
    btnConn.disabled = true;

    const requestOptions = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "username": username,
            "password": pwd
        })
    };

    await fetch(urlApi + "/api/login", requestOptions)
        .then(function (response) {

            //si connexion reussi
            if (response.status == 200) {
                console.log('connecté')
                return response.json()

            }

            //si connexion non authorisé
            else if (response.status == 401) {
                console.log('info invalide')
                //montrer message d'erreur
                messageConn.style.visibility = "visible";
                messageConn.innerHTML = "erreur : informations de connexion invalides"

            }
            else {
                // throw new Error(response.status)

            }
        })
        .then(data => {

            //si connexion reussi et token retourné 
            if (data.token) {
                console.log(data)
                //mettre le token en session
                sessionStorage.setItem("token", data.token)

                //fermer modal et message errreur
                var modal = bootstrap.Modal.getInstance(document.getElementById('modal-conn'))
                modal.hide();
                messageConn.style.visibility = "hidden";

                //recuperer info de l'user:
                recupererinfoUserSiConnecte();
            }

        })
        .catch(function (error) {
            console.log(error)
        });


    btnConn.innerHTML = 'se connecter'
    btnConn.disabled = false;

}

// inscrire user :

//connecter l'utilisateur en envoyant une requete fetch
async function inscrire() {

    btnInsc.innerHTML = '  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> inscription...'
    btnInsc.disabled = true;


    //recuperer info input
    let username = document.getElementById("i-username").value
    let plainPassword = document.getElementById("i-pwd").value


    const requestOptions = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(
            {
                "username": username,
                "plainPassword": plainPassword
            }
        )
    };

    await fetch(urlApi + "/api/users", requestOptions)
        .then(function (response) {

            //si inscription reussi code 201 created
            if (response.status == 201) {
                console.log('inscrit')
                return response.json()

            }

            //si uter existe
            else if (response.status == 500) {
                console.log('info invalide user existe')
                //montrer message d'erreur
                messageInsc.style.visibility = "visible";
                messageInsc.innerHTML = "erreur : username déja utilisé"

            }
            else {
                // throw new Error(response.status)

            }
        })
        .then(data => {

            console.log(data)

            //si inscription reussi connecter l'user et fermer modal
            if (data.username) {
                //afficher un message dans le conteneur principal
                rowPrincipal.insertAdjacentHTML('afterbegin',
                    '<div id="alert-success" class="alert alert-success alert-principal alert-dismissible fade show" role="alert">' +
                    `l'utilisateur <strong>${username}</strong> a été créé , vous pouvez désormais vous connecter ! ` +
                    '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>' +
                    '</div>'
                )
            }

            //fermer modal et message errreur
            var modal = bootstrap.Modal.getInstance(document.getElementById('modal-inscription'))
            modal.hide();
            messageConn.style.visibility = "hidden";



        })
        .catch(function (error) {
            console.log(error)
        });


    btnInsc.innerHTML = "s'inscrire"
    btnInsc.disabled = false;

}



//fonction pour gerer les users si l'user est admin 
async function listerUsers() {

    btnRecharger.innerHTML = '  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> chargement...'
    btnRecharger.disabled = true;


    const requestOptions = {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + sessionStorage.getItem("token"),
        }

    };

    await fetch(urlApi + "/api/users", requestOptions)
        .then(function (response) {
            //si recuperation musdique reussi
            if (response.status == 200) {
                return response.json()
            }
        })
        .then(function (users) {

            let bodymodal = document.getElementById("body-modal-gestion")
            bodymodal.innerHTML=''

            users.forEach(user => {

                //si user n'est pas un admin afficher
                if(!(user.roles.includes("ROLE_ADMIN")) ) {
                    let div = document.createElement('div')
                    div.classList.add("py-2")
                    // ajouter la ligne commanter pour ma modif 
                    // <button onclick="modifierUser(${user.id})" type="button" class="btn btn-success">Modifier</button>
                    div.innerHTML = `
                    <input value=${user.username}>
                    <button onclick="supprimerUser(${user.id})" type="button" class="btn btn-danger  float-end">Supprimer</button>
                    `
                    bodymodal.appendChild(div)
                }

            });


        })

        btnRecharger.innerHTML = "Recharger"
        btnRecharger.disabled = false;
}


//supprimer user
async function supprimerUser(id) {
    //verifier si user connecté :
    let token = sessionStorage.getItem("token")
    if (token) {
        // a faire

        //envoyer la requete de suppression :
        const requestOptions = {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + sessionStorage.getItem("token"),
            }
        };


        await fetch(urlApi + "/api/users/" + id, requestOptions)
            .then(function (response) {
                //si suppresion user
                if (response.status == 204) {
                    
                    console.log("supprimé")
                    //recharger la liste des user
                    listerUsers();
                }
                else {
                    // erreur
                    console.log(response.status)
                }
            })


    };
}

//supprimer user
// async function modifierUser(id) {
//     //verifier si user connecté :
//     let token = sessionStorage.getItem("token")
//     if (token) {
//         // a faire

//         //envoyer la requete de suppression :
//         const requestOptions = {
//             method: 'PATCH',
//             headers: {
//                 'Accept': 'application/json',
//                 'Content-Type': 'application/json',
//                 'Authorization': 'Bearer ' + sessionStorage.getItem("token"),
//             },
//             body : JSON.stringify({
//                 "username": "testmodif"
//             })
//         };


//         await fetch(urlApi + "/api/users/" + id, requestOptions)
//             .then(function (response) {
//                 //si suppresion user
//                 if (response.status == 200) {
                    
//                     console.log("modifié")
//                     //recharger la liste des user
//                     listerUsers();
//                 }
//                 else {
//                     // erreur
//                     console.log(response.status)
//                 }
//             })


//     };
// }
//----------------------------- gestion des musiques ---------------------------------



//fonction pour recuperer toute les musiques
async function recupererMusique(url) {

    //mettre l'url de la page courante demande dans une variable
    // pour le rafraichissement apres suppr ou modification musique 
    console.log(url)
    pageCourante = url
    console.log("recuperation des musique")
    //effacer le conteneur:
    conteneurPrincipal.innerHTML = ""

    const requestOptions = {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    };


    await fetch(urlApi + url)
        .then(function (response) {
            //si recuperation musdique reussi
            if (response.status == 200) {
                return response.json()

            }
        })
        //traiter les donnees recuperés 
        .then(function (data) {

            // //mettre à jour la pagination
            let pagination = data["hydra:view"]
            premierePage.addEventListener('click', () => recupererMusique(pagination["hydra:first"]))
            dernierePage.addEventListener('click', () => recupererMusique(pagination["hydra:last"]))

            if (pagination["hydra:previous"]) {
                pagePrec.classList.remove("disabled")
                pagePrec.disabled = false
                pagePrec.addEventListener('click', () => recupererMusique(pagination["hydra:previous"]))

            } else {
                 pagePrec.classList.add("disabled")
                pagePrec.disabled = true
            }

            if (pagination["hydra:next"]) {
                pageSuivante.classList.remove("disabled")
                pageSuivante.disabled = false
                pageSuivante.addEventListener('click', () => recupererMusique(pagination["hydra:next"]))

            } else {
                pageSuivante.classList.add("disabled")
                pageSuivante.disabled = true
            }

            //lister les musiques

            data["hydra:member"].forEach(musique => {

                // convertir la date 
                let date = new Date(musique.dateSortie).toLocaleDateString("fr-FR")
                //ajouter une card contenant les info de la musique dans le conteneur principal
                conteneurPrincipal.insertAdjacentHTML('beforeEnd',
                    `<div class="card my-2">
            <div class="card-body">
            <div>
            <Strong>${musique.titre}</Strong> 
            <div class="ms-3">Artiste : ${musique.artiste} <br>sortie le : ${date} </div>
            </div>
              
              <div class="float-end">
                <button onclick="afficherModif(${musique.id},'${musique.titre}','${musique.artiste}','${musique.dateSortie}')" 
                type="button" data-bs-toggle="modal"
                data-bs-target="#modal-modif" class="btn btn-success">Modifier</button>
                <button onclick="supprimerMusique(${musique.id})" type="button" class="btn btn-danger">Supprimer</button>
              </div>
            </div>
          </div>`
                )


            });
        }

        )

}


//----------------supprimer une musique par son id 
async function supprimerMusique(id) {
    //verifier si user connecté :
    let token = sessionStorage.getItem("token")
    if (token) {
        // a faire

        //envoyer la requete de suppression :
        const requestOptions = {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + sessionStorage.getItem("token"),
            }
        };


        await fetch(urlApi + "/api/musiques/" + id, requestOptions)
            .then(function (response) {
                //si recuperation musdique reussi
                if (response.status == 204) {
                    //afficher message success musique supprimé
                    rowPrincipal.insertAdjacentHTML('afterbegin',
                        `<div id="alert-danger" class="alert alert-success alert-principal alert-dismissible fade show mx-auto" role="alert">
                    la musqiue ${id} a été supprimée !  
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>`)

                    console.log("supprimé")
                    //recharger la liste des musique
                    recupererMusique(pageCourante)
                }
                else {
                    //afficher message erreur
                    rowPrincipal.insertAdjacentHTML('afterbegin',
                        `<div id="alert-danger" class="alert alert-danger alert-principal alert-dismissible fade show mx-auto" role="alert">
                     erreur lors de la suppression de la musqiue ${id}   
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>`)
                }
            })

    }
    else {
        //afficher message d'erreur    
        rowPrincipal.insertAdjacentHTML('afterbegin',
            `<div id="alert-danger" class="alert alert-danger alert-principal alert-dismissible fade show mx-auto" role="alert">
     Vous devez être connecté pour supprimer une musique !  
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>`)
    }





}


//----------------------modifier la musique------------------------------------- 


//afficher le modale
async function afficherModif(id, titre, artiste, dateSortie) {



    //mettre la date dans le bon format pour input et corriger la date en rajoutant 1j
    let date = new Date(dateSortie)
    console.log(date)
    date.setDate(date.getDate() + 1)

    // afficher les infos dans le modale de modification 
    ModalTitre.textContent = "Modification"
    btnModifier.textContent = "Modifier"
    modalModifTitre.value = titre
    modalModifArtiste.value = artiste
    modalModifdateSortie.value = date.toISOString().substr(0, 10)
    console.log(modalModifdateSortie.value)
    //mettre a jour la fonction click sur le bouton modifier avec l'id recupéré
    btnModifier.addEventListener("click", function () {
        modifierMusique(id)
    })


}

async function modifierMusique(id) {
    //afficher chargement de la modif
    btnModifier.innerHTML = '  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Modification...'
    btnModifier.disabled = true;

    //verifier si user connecté :
    let token = sessionStorage.getItem("token")
    if (token) {
        // a faire

        //envoyer la requete de suppression :

        const requestOptions = {
            method: 'PATCH',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/merge-patch+json',
                'Authorization': 'Bearer ' + sessionStorage.getItem("token"),
            },
            body: JSON.stringify({
                "titre": modalModifTitre.value,
                "artiste": modalModifArtiste.value,
                "dateSortie": modalModifdateSortie.value
            })
        };


        await fetch(urlApi + "/api/musiques/" + id, requestOptions)
            .then(function (response) {
                //si mofification musique reussi
                if (response.status == 200) {
                    //afficher message success musique modifié
                    rowPrincipal.insertAdjacentHTML('afterbegin',
                        `<div id="alert-danger" class="alert alert-success alert-principal alert-dismissible fade show mx-auto" role="alert">
                    la musqiue ${id} a été modidié !  
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>`)

                    console.log("modifié")
                    //recharger la liste des musique
                    recupererMusique(pageCourante)
                }
                else {
                    //afficher message erreur
                    rowPrincipal.insertAdjacentHTML('afterbegin',
                        `<div id="alert-danger" class="alert alert-danger alert-principal alert-dismissible fade show mx-auto" role="alert">
                     erreur lors de la suppression de la musqiue ${id}   
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>`)

                    throw new Error(response.status)


                }
            }).then(function () {
                // fermer le modal de modif 
                var modal = bootstrap.Modal.getInstance(document.getElementById('modal-modif'))
                modal.hide();
            })
            .catch(function (error) {
                console.log(error)
            })

    }
    else {
        //afficher message d'erreur    
        rowPrincipal.insertAdjacentHTML('afterbegin',
            `<div id="alert-danger" class="alert alert-danger alert-principal alert-dismissible fade show mx-auto" role="alert">
     Vous devez être connecté pour modifier une musique !  
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>`)
    }

    btnModifier.innerHTML = "Modifier"
    btnModifier.disabled = false;


}



//--------------------- ajouter une musique----------------------------
//ouvre et modifie le modal de modification pour l'ajout :
//afficher le modale
async function afficherAjout() {

    // changer le nom du bouton modifier en ajout
    ModalTitre.textContent = "Ajout d'une musique"
    btnModifier.textContent = "Ajouter"
    // effacer la valeur des input 
    modalModifTitre.value = null
    modalModifArtiste.value = null
    modalModifdateSortie.value = null

    //mettre a jour la fonction click sur le bouton modifier avec l'id recupéré
    btnModifier.addEventListener("click", function () {
        ajouterMusique()
    })


}


// foonction d'ajout de musique

async function ajouterMusique() {

    btnModifier.innerHTML = '  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Ajout...'
    btnModifier.disabled = true;

    const requestOptions = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + sessionStorage.getItem("token"),
        },
        body: JSON.stringify({
            "titre": modalModifTitre.value,
            "artiste": modalModifArtiste.value,
            "dateSortie": modalModifdateSortie.value
        })
    };

    await fetch(urlApi + "/api/musiques", requestOptions)
        .then(function (response) {

            //si ajout reussi code 201 created
            if (response.status == 201) {
                console.log('ajouté')
                //afficher un message dans le conteneur principal
                rowPrincipal.insertAdjacentHTML('afterbegin',
                    '<div id="alert-success" class="alert alert-success alert-principal alert-dismissible fade show" role="alert">' +
                    `l'utilisateur <strong>${modalModifTitre.value}</strong> de <strong>${modalModifArtiste.value}</strong> a été ajoutée ! ` +
                    '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>' +
                    '</div>'
                )

                //fermer modal et message errreur
                var modal = bootstrap.Modal.getInstance(document.getElementById('modal-modif'))
                modal.hide();
                messageConn.style.visibility = "hidden";
            }
            else {
                throw new Error(response.status)

            }
        })
        .catch(function (error) {
            console.log(error)
        });


    btnModifier.innerHTML = "Ajouter"
    btnModifier.disabled = false;

}


//-------------------- fonction rechercher par artiste-----------------------------------

function rechercher() {
    let q = document.getElementById('input_search').value
    recupererMusique("/api/musiques?artiste="+q)
}

//lancer la recuperation 
recupererMusique("/api/musiques")




//rendre les fonction global pour corriger le probleme de fonction non defini dans les onclick des bouton causé par webpack
window.supprimerMusique = supprimerMusique;
window.afficherModif = afficherModif;
window.modifierMusique = modifierMusique;
window.afficherAjout = afficherAjout;

window.supprimerUser = supprimerUser;
// window.modifierUser = modifierUser;
window.listerUsers = listerUsers;

// associer action aux bouton :
btnConn.addEventListener("click", () => connecter(document.getElementById("username").value, document.getElementById("pwd").value))
btnInsc.addEventListener("click", inscrire)
btnGestionUsers.addEventListener("click", listerUsers)
btnEffacer.addEventListener('click',() => recupererMusique("/api/musiques"))
btnSearch.addEventListener('click',rechercher)