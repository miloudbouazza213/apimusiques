# Information 
coté api j'ai utilisé symfony avec api plateform , il y 'a deux entités:
une entité Musique :Titre , auteur , DateCreation
une entité User : username ,password , plainPassword

coté client: un client node en single page 

# probleme cors origin du client js sur gitpod Solution :
aller dans remote explorer et mettre les port sur public

# installation (dans api-symfony)
## dependence
composer install 
## base de données :
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
## generation de donnee
generation de donnée avec faker et creation d'un admin username et mdp = admin
php bin/console doctrine:fixtures:load 

# installation client js:
npm install 

# lancement serveurs:
## api 
symfony server:start
## client 
node server.js


# docker commande
pour construire les container :
docker-compose up -d --build

#entrer dans le conteneur php-8 ou se trouve symfony (avec l'utilisateur root car probleme de permission)
docker exec -u 0 -it container_php-8 bash



## commande utile symfony
symfony server:start
## commande utiles docker
supprimer les conteneur :
docker rm $(docker ps -aq) --force

supprimer les images docker 
docker rmi $(docker images -q)